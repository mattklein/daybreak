"""OAuth and calendar services for the application."""

from collections import namedtuple
import json

from flask.helpers import url_for
from rauth.service import OAuth2Service
from werkzeug.exceptions import BadRequest
from werkzeug.utils import redirect

from daybreak import settings

# common representation of user credentials amongst different OAuth sevices
Credentials = namedtuple('Credentials', ['social_id', 'nickname', 'email', 'token'])

class CalendarSource:
	"""Represents a source of calendars and events."""

	def get_calendars(self):
		"""Retrieves calendars from the service."""
		raise NotImplementedError()

class OAuthService:
	"""Represents an OAuth-based service.
	
	Implementations of this class may be looked-up from a central dict, using the get_provider() class method."""

	services = None  # A singleton lookup for services, keyed by the service name.

	def __init_subclass__(cls, name, secrets=None, **kwargs):
		cls.name = name

		# fetch secrets from settings automatically
		if secrets is None:
			secrets = settings.SECRETS[name]

		cls.service = OAuth2Service(
			name=name,
			client_id=secrets['id'],
			client_secret=secrets['secret'],
			**kwargs
		)

	def authorize(self):
		"""Begins authorization with the given OAuth service."""
		raise NotImplementedError()

	def callback(self, code):
		"""Given an once-off code from a successful authorization, yields OAuth credentials for this provider that may 
		be stored for later use and access."""
		raise NotImplementedError()

	def _get_callback_url(self):
		"""Retrieves the callback URL for this OAuth provider."""
		return url_for('oauth_callback', provider=self.name, _external=True)

	@classmethod
	def get_service(cls, name):
		"""Attempts to retrieve a service with the given name, and raises a BadRequest error if it is not available."""
		instance = cls.try_get_service(name)

		if instance is None:
			raise BadRequest('Provider is not recognized: ' + name)

		return instance

	@classmethod
	def try_get_service(cls, name):
		"""Retrieves a sub-classed service with the given name, or None if it's not available."""
		if cls.services is None:
			cls.services = {}
			for service_class in cls.__subclasses__():
				service = service_class()
				cls.services[service.name] = service

		return cls.services.get(name)

class GoogleOAuthService(OAuthService, CalendarSource, name='google',
												 base_url='https://www.googleapis.com/',
												 authorize_url='https://accounts.google.com/o/oauth2/auth',
												 access_token_url='https://accounts.google.com/o/oauth2/token'):
	def authorize(self):
		# authorize with the service, permitting calendar and email access
		authorize_url = self.service.get_authorize_url(
			scope=' '.join([
				'https://www.googleapis.com/auth/calendar',
				'https://www.googleapis.com/auth/userinfo.email',
				'https://www.googleapis.com/auth/userinfo.profile',
			]),
			access_type='offline',
			response_type='code',
			redirect_uri=self._get_callback_url()
		)
		return redirect(authorize_url)

	def callback(self, code):
		# retrieve an access token for the user
		access_token = self.service.get_access_token(
			key='access_token',
			decoder=json.loads,
			data={
				'code':         code,
				'redirect_uri': self._get_callback_url(),
				'grant_type':   'authorization_code',
			}
		)

		# query for the user's details
		session = self.service.get_session(access_token)
		response = session.get('/oauth2/v1/userinfo?alt=json').json()
		id, given_name, family_name, email = map(response.get, ('id', 'given_name', 'family_name', 'email'))

		return Credentials(
			social_id='google$' + id,
			nickname=given_name + ' ' + family_name,
			email=email,
			token=access_token,
		)

class OutlookOAuthService(OAuthService, CalendarSource, name='outlook',
													authorize_url='https://login.microsoftonline.com/common/oauth2/v2.0/authorize',
													access_token_url='https://login.microsoftonline.com/common/oauth2/v2.0/token'):
	def authorize(self):
		authorize_url = self.service.get_authorize_url(
			scope='calendars.read',
			response_type='code'
		)
		return redirect(authorize_url)

class TodoistOAuthService(OAuthService, CalendarSource, name='todoist',
													authorize_url='https://todoist.com/oauth/authorize',
													access_token_url='https://todoist.com/oauth/access_token'):
	def authorize(self):
		authorize_url = self.service.get_authorize_url(
			scope='data:read',
			response_type='code',
		)
		return redirect(authorize_url)

class GithubOAuthService(OAuthService, CalendarSource, name='github',
												 authorize_url='https://github.com/login/oauth/authorize',
												 access_token_url='https://github.com/login/oauth/access_token'):
	def authorize(self):
		authorize_url = self.service.get_authorize_url(
			scope='user',
			response_type='code',
			redirect_uri=self._get_callback_url()
		)
		return redirect(authorize_url)
