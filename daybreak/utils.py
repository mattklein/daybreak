"""Common utilities for the application."""

import base64
from functools import partial, wraps
from itertools import cycle
import pickle

from flask.json import jsonify
from flask.wrappers import Response
from redis.client import Redis

from daybreak import settings

seconds = 1000
minutes = seconds * 60
hours = minutes * 60

class RedisCache:
	"""A redis-backed cache for a user data."""
	redis = Redis.from_url(settings.REDIS_URL)

	def __init__(self, keyfunc=None):
		""":param keyfunc A function used to transform the key used when using the decorator pattern on the cache."""
		if keyfunc is None:
			keyfunc = identity

		self.keyfunc = keyfunc

	def __call__(self, key, expire=None):
		"""Produces a decorator that caches the result of the associated function in Redis."""

		def decorator(func):
			@wraps(func)
			def wrapper(*args, **kwargs):
				# cache the result from the function call in Redis
				innerkey = self.keyfunc(key)  # compute a key based on the decorator args
				builder = partial(func, *args, **kwargs)  # apply wrapper arguments
				return self.get_or_compute(innerkey, builder, expire)

			return wrapper

		return decorator

	def get_or_compute(self, key, builder, expire=None):
		"""Attempts to retrieve the value with the given key from the cache.
		
		If the value does not exist, uses the given builder to produce it.
		The values are pickled and unpickled from redis to preserve identity.
		
		If expire is specified, the contents are expired after the duration, in milliseconds."""
		result = self.redis.get(key)

		if result is None:
			result = builder()
			pickled = pickle.dumps(result)
			self.redis.set(key, pickled, px=expire)
		else:
			return pickle.loads(result)

		return result

def identity(value):
	"""The identity function; returns the input verbatim."""
	return value

def json_response(func):
	"""Wraps a function with another that yields a JSON response."""

	@wraps(func)
	def wrapper(*args, **kwargs):
		response = func(*args, **kwargs)

		# pass responses through directly.
		if isinstance(response, Response):
			return response

		# otherwise, json-ify the results
		return jsonify(response)

	return wrapper

def encrypt_string(plaintext, key=settings.SECRET_KEY):
	"""Given plain text and a key, encrypts the given text."""
	return base64.b64encode(bytes(''.join(_xor_cipher(plaintext, key)), encoding='utf8'))

def decrypt_string(ciphertext, key=settings.SECRET_KEY):
	"""Given ciphered text and a key, decrypts the text."""
	return base64.b64decode(bytes(''.join(_xor_cipher(ciphertext, key)), encoding='utf8'))

def _xor_cipher(value, key):
	"""A simple cipher that xors the given values with the given key."""
	return [chr(ord(x) ^ ord(y)) for (x, y) in zip(value, cycle(key))]
