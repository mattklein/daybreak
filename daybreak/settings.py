"""Common settings and secrets for the Daybreak application."""

import os.path

from dotenv.main import load_dotenv

# load configuration from .env file, if it's present
dotenv_path = os.path.join(os.path.dirname(__file__), '../.env')
if os.path.exists(dotenv_path):
	load_dotenv(dotenv_path)

# common settings
VERSION = os.environ.get('VERSION', '1')
PORT = int(os.environ.get('PORT', '8000'))
HOST = os.environ.get('HOST', 'localhost')
DEBUG = os.environ.get('DEBUG', 'false') == 'true'
SECRET_KEY = os.environ.get('SECRET_KEY', 'a1b2c3d4e5')

# database settings
DATABASE_URL = os.environ.get('DATABASE_URL', 'postgresql://postgres@localhost:5432/daybreak')
REDIS_URL = os.environ.get('REDIS_URL', 'redis://localhost:6379/0')

# secrets for various service providers
SECRETS = {
	'outlook': {
		'id':     os.environ.get('OUTLOOK_CLIENT_ID'),
		'secret': os.environ.get('OUTLOOK_CLIENT_SECRET'),
	},
	'google':  {
		'id':     os.environ.get('GOOGLE_CLIENT_ID'),
		'secret': os.environ.get('GOOGLE_CLIENT_SECRET'),
	},
	'todoist': {
		'id':     os.environ.get('TODOIST_CLIENT_ID'),
		'secret': os.environ.get('TODOIST_CLIENT_SECRET'),
	},
	'github':  {
		'id':     os.environ.get('GITHUB_CLIENT_ID'),
		'secret': os.environ.get('GITHUB_CLIENT_SECRET'),
	},
}
