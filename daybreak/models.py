"""The database models for the Daybreak application."""

from flask_login.mixins import UserMixin
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql.sqltypes import DateTime, Integer, String

from daybreak.utils import decrypt_string, encrypt_string

db = SQLAlchemy()  # the root SQLAlchemy instance

class User(db.Model, UserMixin):
	"""Represents a user of the appliation."""
	__tablename__ = 'users'

	id = db.Column(Integer(), primary_key=True)
	social_id = db.Column(String(64), unique=True)
	nickname = db.Column(String(255), nullable=True)
	email = db.Column(String(255), nullable=True)
	connections = db.relationship('Connection', backref='user', lazy='dynamic')

	def __init__(self, social_id, nickname, email):
		self.social_id = social_id
		self.nickname = nickname
		self.email = email

	def __str__(self):
		return f"<User {self.email}>"

class Connection(db.Model):
	"""Represents a connection to a third-party OAuth service."""
	__tablename__ = 'connections'

	id = db.Column(Integer(), primary_key=True)
	user_id = db.Column(Integer(), db.ForeignKey('users.id'))
	service = db.Column(String(64))
	encrypted_token = db.Column(String())
	expires_at = db.Column(DateTime(), nullable=True)

	def __init__(self, user, service, token, expires_at=None):
		self.user = user
		self.service = service
		self.token = token
		self.expires_at = expires_at

	@property
	def token(self):
		return decrypt_string(self.encrypted_token)

	@token.setter
	def token(self, value):
		self.encrypted_token = encrypt_string(value)

	def __str__(self):
		return f"<Connection {self.service}>"
