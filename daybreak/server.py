"""The primary flask server for the Daybreak application."""

from flask import Flask, redirect
from flask.globals import request
from flask.helpers import send_from_directory, url_for
from flask.templating import render_template
from flask_login.login_manager import LoginManager
from flask_login.utils import current_user, login_required, login_user, logout_user
from werkzeug.exceptions import BadRequest

from daybreak import settings
from daybreak.models import Connection, User, db
from daybreak.services import OAuthService
from daybreak.utils import json_response

app = Flask(__name__)
app.config['SECRET_KEY'] = settings.SECRET_KEY
app.config['SQLALCHEMY_DATABASE_URI'] = settings.DATABASE_URL
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

login_manager = LoginManager(app)

db.init_app(app)

@app.route('/')
@login_required
def index():
	"""Renders the main application."""
	return render_template('index.html', events=[])

@app.route("/calendars/<string:provider>")
@login_required
@json_response
def get_calendars(provider):
	"""Retrieves all of the user's calendars."""
	service = OAuthService.get_service(provider)
	return service.get_calendars()

@app.route('/login', methods=['GET', 'POST'])
def login():
	"""Handles the login process by displaying the main login page."""
	if not current_user.is_anonymous:
		return redirect(url_for('calendar'))

	# if this is a post-back, begin the OAuth process with google
	if request.method == 'POST':
		oauth_service = OAuthService.get_service('google')
		return oauth_service.authorize()

	# otherwise, just render the login page
	return render_template('login.html')

@app.route('/logout')
@login_required
def logout():
	"""Logs a user out of the application."""
	logout_user()
	return redirect(url_for('login'))

@app.route('/oauth/<string:provider>')
@login_required
def oauth_authorize(provider):
	"""Authenticates a user with an OAuth service."""
	oauth_service = OAuthService.get_service(provider)
	return oauth_service.authorize()

@app.route('/oauth/callback/<string:provider>')
def oauth_callback(provider):
	"""Callback for succesful OAuth connection."""
	oauth_service = OAuthService.get_service(provider)

	# we should receive a once-off code for the user
	if 'code' not in request.args:
		raise BadRequest('The request was denied.')

	code = request.args['code']

	# get credentials from the service
	credentials = oauth_service.callback(code)
	social_id, nickname, email, token = credentials

	# get or create the associated local user account
	user = User.query.filter_by(social_id=social_id).first()
	if user is None:
		user = User(social_id=social_id, nickname=nickname, email=email)
		db.session.add(user)

	# get or create the associated connection entry
	connection = Connection(user=user, service=provider, token=token)
	db.session.add(connection)

	# persist changes to the database
	db.session.commit()

	login_user(user, remember=True)
	return redirect(url_for('index'))

@login_manager.user_loader
def load_user(id):
	"""Loads a user from the database given an identifier in session storage."""
	return User.query.get(int(id))

@login_manager.unauthorized_handler
def unauthorized():
	"""Handles an unauthorized request."""
	return redirect(url_for('login'))

@app.route('/<path:path>')
def serve_static_file(path):
	"""Serve static files directly."""
	return send_from_directory('static', path)

@app.errorhandler(404)
def page_not_found(error):
	"""Handles 404 not found errors."""
	return render_template('errors/4xx.html'), 404

@app.errorhandler(500)
def internal_server_error(error):
	"""Handles 500 internal server errors."""
	return render_template('errors/5xx.html'), 500

if __name__ == '__main__':
	# create database tables
	with app.app_context():
		db.create_all()

	# start the application server
	app.run(
		host=settings.HOST,
		port=settings.PORT,
		debug=settings.DEBUG,
	)
